package com.example.stretchingapp.data

import androidx.annotation.Keep

@Keep
class StretchSplashResponse(val url : String)