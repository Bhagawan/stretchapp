package com.example.stretchingapp.data

import androidx.annotation.Keep

@Keep
data class FullStretch(val img: String, val reason: String, val stretchings: List<Stretch>)
