package com.example.stretchingapp.data

import androidx.annotation.Keep

@Keep
data class Stretch(val header: String, val description: String)
