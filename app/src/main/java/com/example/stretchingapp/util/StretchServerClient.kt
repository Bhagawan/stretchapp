package com.example.stretchingapp.util

import com.example.stretchingapp.data.FullStretch
import com.example.stretchingapp.data.StretchSplashResponse
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

interface StretchServerClient {

    @FormUrlEncoded
    @POST("StretchApp/splash.php")
    fun getSplash(@Field("locale") locale: String
                  , @Field("simLanguage") simLanguage: String
                  , @Field("model") model: String
                  , @Field("timezone") timezone: String): Call<StretchSplashResponse>

    @GET("StretchApp/{filename}")
    fun getStretch(@Path("filename") file: String): Call<FullStretch>

    companion object {
        fun create() : StretchServerClient {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://195.201.125.8/")
                .build()
            return retrofit.create(StretchServerClient::class.java)
        }
    }

}