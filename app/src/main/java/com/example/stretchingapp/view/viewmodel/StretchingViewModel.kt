package com.example.stretchingapp.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import com.example.stretchingapp.BR
import com.example.stretchingapp.data.FullStretch
import com.example.stretchingapp.data.Stretch
import com.example.stretchingapp.util.StretchServerClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class StretchingViewModel(file: String): BaseObservable() {

    @Bindable
    var imageUrl = ""

    @Bindable
    var reason = ""

    @Bindable
    var stretchings: List<Stretch> = emptyList()

    val error = MutableLiveData<String>()
    val back = MutableLiveData<Boolean>()

    init {
        StretchServerClient.create().getStretch(file).enqueue(object : Callback<FullStretch> {
            override fun onResponse(
                call: Call<FullStretch>,
                response: Response<FullStretch>
            ) {
                if(response.isSuccessful && response.body() != null) {
                    imageUrl = response.body()!!.img
                    reason = response.body()!!.reason
                    stretchings = response.body()!!.stretchings
                    notifyPropertyChanged(BR.imageUrl)
                    notifyPropertyChanged(BR.reason)
                    notifyPropertyChanged(BR.stretchings)
                } else error.postValue("Ошибка доступа к серверу")
            }
            override fun onFailure(call: Call<FullStretch>, t: Throwable) {
                error.postValue("Ошибка доступа к серверу")
            }
        })
    }

    fun backPress() {
        back.postValue(true)
    }


}