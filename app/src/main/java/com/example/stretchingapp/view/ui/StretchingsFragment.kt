package com.example.stretchingapp.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.stretchingapp.databinding.FragmentStrechingsBinding
import com.example.stretchingapp.view.adapter.ItemAdapter
import com.example.stretchingapp.view.viewmodel.StretchingViewModel

class StretchingsFragment(private val file: String) : Fragment() {
    private lateinit var binding: FragmentStrechingsBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentStrechingsBinding.inflate(layoutInflater)
        binding.viewModel = StretchingViewModel(file)
        (binding.viewModel as StretchingViewModel).error.observe(viewLifecycleOwner) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
            back()
        }
        binding.recyclerStretch.layoutManager = LinearLayoutManager(context)
        binding.recyclerStretch.adapter = ItemAdapter()
        (binding.viewModel as StretchingViewModel).back.observe(viewLifecycleOwner) {
            back()
        }
        return binding.root
    }

    private fun back() {
        parentFragmentManager.popBackStack()
    }
}