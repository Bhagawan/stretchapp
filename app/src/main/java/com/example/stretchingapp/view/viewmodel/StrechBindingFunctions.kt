package com.example.stretchingapp.view.viewmodel

import android.util.Log
import android.view.View
import android.view.animation.Animation
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.stretchingapp.R
import com.example.stretchingapp.data.Stretch
import com.example.stretchingapp.view.adapter.ItemAdapter
import com.squareup.picasso.Picasso

@BindingAdapter("imageUrl")
fun setImageFromUrl(v: ImageView, url: String?) {
    if(!url.isNullOrBlank()) Picasso.get().load(url).into(v)
}

@BindingAdapter("recyclerData")
fun setRecyclerData(v: RecyclerView, data: List<Stretch>?) {
    if(v.adapter != null && data != null){
        if(v.adapter is ItemAdapter) {
            (v.adapter as ItemAdapter).setData(data)
        }
    }
}

@BindingAdapter("visible")
fun setVisibility(v: View, visible: Boolean) {
    if(visible) {
        val anim = android.view.animation.AnimationUtils.loadAnimation(v.context, R.anim.anim_slide_in)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) {
                v.visibility = View.VISIBLE
            }
            override fun onAnimationEnd(animation: Animation?) { }
            override fun onAnimationRepeat(animation: Animation?) { }
        })
        v.startAnimation(anim)

    } else {
        val anim = android.view.animation.AnimationUtils.loadAnimation(v.context, R.anim.anim_slide_out)
        anim.setAnimationListener(object : Animation.AnimationListener {
            override fun onAnimationStart(animation: Animation?) { }
            override fun onAnimationEnd(animation: Animation?) {
                v.visibility = View.GONE
            }
            override fun onAnimationRepeat(animation: Animation?) { }
        })
        v.startAnimation(anim)
    }
}

@BindingAdapter("rotation")
fun rotateView(v: View, open: Boolean) {
    if(v.animation != null) v.clearAnimation()
    if(open) v.startAnimation(android.view.animation.AnimationUtils.loadAnimation(v.context, R.anim.anim_open))
    else v.startAnimation(android.view.animation.AnimationUtils.loadAnimation(v.context, R.anim.anim_close))
}

class ArchBindingFunctions {}