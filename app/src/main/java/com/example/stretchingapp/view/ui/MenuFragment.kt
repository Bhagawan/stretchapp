package com.example.stretchingapp.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.stretchingapp.R
import com.example.stretchingapp.databinding.FragmentMenuBinding
import com.example.stretchingapp.view.viewmodel.MenuViewModel

class MenuFragment : Fragment() {
   private lateinit var binding: FragmentMenuBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentMenuBinding.inflate(layoutInflater)
        binding.viewModel = MenuViewModel()
        (binding.viewModel as MenuViewModel).stretch.observe(viewLifecycleOwner) {
            if(!it.isNullOrBlank())
                parentFragmentManager.beginTransaction().replace(R.id.fragmentContainerView, StretchingsFragment(it)).addToBackStack("").commit()
        }
        return binding.root
    }
}