package com.example.stretchingapp.view.viewmodel

import android.view.View
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.example.stretchingapp.BR
import com.example.stretchingapp.data.Stretch

class ItemViewModel(val item: Stretch) :BaseObservable() {


    @Bindable
    var descriptionVisibility = false


    fun onClick(v: View) {
        descriptionVisibility = !descriptionVisibility
        notifyPropertyChanged(BR.descriptionVisibility)
    }
}