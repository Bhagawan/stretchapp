package com.example.stretchingapp.view.viewmodel

import androidx.databinding.BaseObservable
import androidx.lifecycle.MutableLiveData

class MenuViewModel: BaseObservable() {

    companion object {
        const val  RUN_STRETCH = "cardio_stretch.json"
        const val  POWER_STRETCH = "power_stretch.json"
        const val  DYNAMIC_STRETCH= "dynamical_stretch.json"
        const val  JOINT_STRETCH = "joint_stretch.json"
        const val  GENERAL_STRETCH = "general_stretch.json"
    }

    val stretch = MutableLiveData("")

    fun goTo(type: String) {
        stretch.postValue(type)
    }
}