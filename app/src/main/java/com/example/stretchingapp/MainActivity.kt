package com.example.stretchingapp

import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.stretchingapp.databinding.ActivityMainBinding
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import java.lang.Exception

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var target: Target

    override fun onBackPressed() {
        if(supportFragmentManager.backStackEntryCount > 0) supportFragmentManager.popBackStack()
        else super.onBackPressed()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        loadBackground()
        setContentView(binding.root)
    }

    private fun loadBackground() {
        target = object : Target {
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                bitmap?.let {
                    binding.root.background = BitmapDrawable(resources, it)
                }
            }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {  }
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) {  }
        }

        Picasso.get().load("http://195.201.125.8/StretchApp/back.png").into(target)
    }
}